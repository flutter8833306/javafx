package com.farm.javaFiles.Screens;

import com.farm.javaFiles.functions.AppBar;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class HomePage {

    Stage stage ;

    
    public HomePage( Stage stage ){
        this.stage = stage ;
    }


    public void homeScreen(){

        AppBar appbarObj  = new AppBar(stage);
        BorderPane appBar = appbarObj.getToolBar();

        ImageView animalHusbandryImage  = takeImage(new  Image("assets/images/screensImage/animalHusbands.jpeg"));
        Label animalHusbandry = getLabel("Animal Husbandry");
        VBox animalHusbandryVBox = getVBox(animalHusbandryImage, animalHusbandry);
        Button animalHusbandryButton = getButton(animalHusbandryVBox);

        animalHusbandryButton.setOnAction(e->{

           AnimalHusbandry obj = new AnimalHusbandry(stage );
           obj.getAnimalsScreen();
            
        });
        

        ImageView AgreeNeewsImage  = takeImage(new  Image("assets/images/screensImage/agreenews1.jpeg")); 
        Label AgreeNeews = getLabel("Agri News");

        VBox AgreeNeewsVbox = getVBox(AgreeNeewsImage, AgreeNeews);
        Button agreeNewsButton = getButton(AgreeNeewsVbox);


        ImageView farmImplementsImage  = takeImage(new  Image("assets/images/screensImage/equipment2.jpeg"));
        Label farmImplements = getLabel("Farm Implements");
        VBox farmImplementsVBox = getVBox(farmImplementsImage, farmImplements);
        Button farmImplementButton  = getButton(farmImplementsVBox);

        farmImplementButton.setOnAction(e->{

           FarmImplements obj = new FarmImplements(stage);
           obj.getfarmImplements();
        });


        HBox hb1 = new HBox(150,animalHusbandryButton,agreeNewsButton,farmImplementButton);
        hb1.setPadding(new Insets(0,0,0,100));


        

        ImageView whetherImage  = takeImage(new  Image("assets/images/screensImage/unnamed.jpg"));
        Label whether = getLabel("Whether Forcast");
        VBox whetherVBox = getVBox(whetherImage, whether);
        Button whetherButton = getButton(whetherVBox);

        whetherButton.setOnAction(e->{
            WeatherScreen obj = new WeatherScreen(stage);
            obj.getWeatherScreen();
        });

        HBox hb2 = new HBox(150,whetherButton);
        hb2.setPadding(new Insets(0,0,0,100));


        
        
       



       
        
      
       





        Image img = new Image("assets/images/screensImage/background.jpeg");
        ImageView backgroundImageView = new ImageView(img);
        backgroundImageView.setOpacity(0.4);
        backgroundImageView.setFitHeight(900);
        backgroundImageView.setFitWidth(1470);
        

    
        VBox finalVBox = new VBox(50,appBar ,hb1,hb2);
        // finalVBox.setLayoutX(450);
        finalVBox.setPadding(new Insets(10,0,0,50));




        StackPane st = new StackPane(backgroundImageView,finalVBox);
        

      
        


       
       

       

       
        Group gp = new Group(st);
      
        



        Scene scene = new Scene(gp);


        stage.setScene(scene);
        stage.setTitle("LoginPage");
        stage.setHeight(900);
        stage.setWidth(1470);
        stage.setX(0);
        stage.setY(0);
        stage.show();
    }


    Label getLabel(String name){
        Label lb = new Label(name);
        lb.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        lb.setStyle("-fx-background-color: #299617; -fx-text-fill:white;-fx-background-radius:0 0 15 15");
        lb.setMaxWidth(270);
        lb.setPrefHeight(40);
        lb.setAlignment(Pos.CENTER);

        return lb ;
    }

    public ImageView takeImage(Image image){
        ImageView img = new ImageView(image);
        img.setFitWidth(270);
        img.setFitHeight(250);
        img.setStyle("-fx-background-color: white; -fx-border-color: transparent");
      
        return img ;
    }


    VBox getVBox(ImageView iv , Label lb){
        VBox vb = new VBox(iv, lb);
        vb.setAlignment(Pos.CENTER);
        return vb ;
    }


    Button getButton( VBox vb){
        Button bt = new Button();
        bt.setGraphic(vb);
        bt.setStyle("-fx-background-color: transparent; -fx-border-color: transparent");


    
        // bt.setPadding(new Insets(20));
       // bt.setStyle("-fx-background-color:GREEN");
        

        return bt ;
    }
    
    
}
