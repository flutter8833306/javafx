package com.farm.javaFiles.Screens;

import com.farm.javaFiles.functions.SearchAppBar;

import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class FarmImplements {

    Stage stage;

     FarmImplements(Stage stage){

        this.stage =stage;

    }  

    void getfarmImplements(){


        SearchAppBar searchObj = new SearchAppBar(stage);
        BorderPane farmBorderPane =   searchObj.getToolBar();
        

        AnimalHusbandry obj = new AnimalHusbandry(stage);
        
        ImageView tractorImageView = obj.takeImage(new Image("assets/images/farmImplements/tractor.jpeg"));
        Label tractor = obj.getNameLabel("ट्रॅक्टर");
        Label tractorPrice = obj.getPriceLabel("₹800/एकर");
        Label tractorVilage = obj.getVillageLabel("गाव.तरंगफळ");

        VBox tractorVBox = obj.getVBox(tractorImageView,  tractor, tractorPrice,tractorVilage);


       
        
        ImageView poclaneImageView = obj.takeImage(new Image("assets/images/farmImplements/poclane.jpeg"));
        Label poclane = obj.getNameLabel("पोकलेन");
        Label poclanePrice = obj.getPriceLabel("₹3000/तास");
        Label poclaneVilage = obj.getVillageLabel("गाव.जालना");


        VBox poclaneVBox = obj.getVBox(poclaneImageView, poclane, poclanePrice, poclaneVilage);



        ImageView jcbImageView = obj.takeImage(new Image("assets/images/farmImplements/jcb.jpeg"));
        Label jcb = obj.getNameLabel("जेसीबी");
        Label jcbPrice = obj.getPriceLabel("₹1000/तास");
        Label jcbVilage = obj.getVillageLabel("गाव.भुसावळ");


        VBox jcbVBox = obj.getVBox(jcbImageView, jcb, jcbPrice, jcbVilage);


        HBox hb1 = new HBox(150,tractorVBox,poclaneVBox,jcbVBox);
        hb1.setPadding(new Insets(0,0,0,100));



        ImageView rotorImageView = obj.takeImage(new Image("assets/images/farmImplements/rotar.jpeg"));
        Label rotor= obj.getNameLabel("रोटर");
        Label rotorPrice = obj.getPriceLabel("₹800/एकर");
        Label rotorVilage = obj.getVillageLabel("गाव.पंढरपूर");


        VBox rotorVBox = obj.getVBox(rotorImageView, rotor, rotorPrice, rotorVilage);



        ImageView pikupImageView = obj.takeImage(new Image("assets/images/farmImplements/pikup.jpeg"));
        Label pikup= obj.getNameLabel("महिन्द्रा बोलेरो");
        Label pikupPrice = obj.getPriceLabel("₹1500/दिवस");
        Label pikupVilage = obj.getVillageLabel("गाव.लातूर");


        VBox pikupVBox = obj.getVBox(pikupImageView, pikup, pikupPrice, pikupVilage);


        ImageView harvestorImageView = obj.takeImage(new Image("assets/images/farmImplements/harvestiong.jpeg"));
        Label harvestor= obj.getNameLabel("कापणी यंत्र");
        Label harvestorPrice = obj.getPriceLabel("₹2000/एकर");
        Label harvestorVilage = obj.getVillageLabel("गाव.जालना");


        VBox harvestorVBox = obj.getVBox(harvestorImageView, harvestor, harvestorPrice, harvestorVilage);


        HBox hb2 = new HBox(150,rotorVBox,pikupVBox,harvestorVBox);
        hb2.setPadding(new Insets(0,0,0,100));




        VBox finalVBox = new VBox(50,hb1,hb2);
        // finalVBox.setLayoutX(450);
        finalVBox.setPadding(new Insets(10,0,0,0));

         ScrollPane scrollPane = new ScrollPane(finalVBox);
        scrollPane.setPadding(new Insets(10));
        
        // Optional: Customize scroll pane properties
        scrollPane.setFitToWidth(true); // Ensure content width fits the ScrollPane width
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER); // Hide horizontal scrollbar if not needed
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED); // Show vertical scrollbar as needed

        VBox vb = new VBox(farmBorderPane,scrollPane);







       



        // Group gp = new Group(farmBorderPane);
        Scene scene = new Scene(vb);
        stage.setScene(scene);
        stage.setTitle("LoginPage");
        stage.setHeight(900);
        stage.setWidth(1470);
        stage.setX(0);
        stage.setY(0);
        stage.show();


    }



    
    
}
