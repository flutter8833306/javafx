package com.farm;


import java.io.FileInputStream;
import java.io.IOException;

import com.farm.javaFiles.loginPage.AgricultureUI;
import com.farm.javaFiles.loginPage.StartPage;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

import javafx.application.Application;

import javafx.stage.Stage;
public class Main  extends Application {

  
    

    @Override
    public void start(Stage primaryStage) throws Exception {

           try{
            FileInputStream serviceAccount = new FileInputStream("farmingproject\\src\\main\\resources\\fx-auth-fb.jason");


             FirebaseOptions options = FirebaseOptions.builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://javafx-project1-83368-default-rtdb.asia-southeast1.firebasedatabase.app")
                .build();
            FirebaseApp.initializeApp(options);


        } catch (IOException e){
                e.printStackTrace();
        }

     AgricultureUI obj = new AgricultureUI(primaryStage);
     obj.startScreen();
    //     // StartPage obj = new StartPage(primaryStage);
    //     // obj.start();
    //     // System.out.println(this);

        

    //     // SearchAppBar obj = new SearchAppBar(primaryStage);
    //     // BorderPane bp =  obj.getToolBar();

    //     // Group gp = new Group(bp);
    //     //  Scene scene = new Scene(gp);
    //     // primaryStage.setScene(scene);
    //     // primaryStage.setTitle("LoginPage");
    //     // primaryStage.setHeight(900);
    //     // primaryStage.setWidth(1470);
    //     // primaryStage.setX(0);
    //     // primaryStage.setY(0);
    //     // primaryStage.show();

    //     // AnimalHusbandry obj = new AnimalHusbandry(primaryStage, new HomePage(primaryStage));
    //     // obj.getAnimalsScreen();

    //     // WeatherScreen obj = new WeatherScreen(primaryStage);
    //     // obj.getWeatherScreen();
    //     // System.out.println(this);



      



     }
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Application.launch(args);
    }
}